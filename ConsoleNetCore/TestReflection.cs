using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleNetCore
{
    public static class TestReflection
    {
        public static void TestPrimitive()
        {
            var listTypeToTest = new List<Type>
            {
                typeof(int), typeof(char), typeof(bool), typeof(string), typeof(TestEnum), typeof(TestStruct), typeof(TestClass),
                typeof(int[]), typeof(List<int>), typeof(IEnumerable<int>)
            };

            foreach (var type in listTypeToTest)
            {
                Console.WriteLine($"Type: {type.Name} IsPrimitive: {IsPrimitive(type)}");
            }
            
            Console.WriteLine();
            
            foreach (var type in listTypeToTest)
            {
                Console.WriteLine($"Type: {type.Name} IsValueType: {IsValueType(type)}");
            }
            
            Console.WriteLine();
            
            foreach (var type in listTypeToTest)
            {
                Console.WriteLine($"Type: {type.Name} IsClass: {IsClass(type)}");
            }
            
            Console.WriteLine();
            
            foreach (var type in listTypeToTest)
            {
                Console.WriteLine($"Type: {type.Name} IsArray: {IsArray(type)}");
            }
            
            Console.WriteLine();
            
            foreach (var type in listTypeToTest)
            {
                Console.WriteLine($"Type: {type.Name} IsCollectible: {IsCollectible(type)}");
            }
            
            Console.WriteLine();
            
            foreach (var type in listTypeToTest)
            {
                Console.WriteLine($"Type: {type.Name} IsIEnumerable: {IsIEnumerable(type)}");
            }
            
            
        }

        public static void TestListType()
        {
            var data = new int[] {1};

            var type = data.GetType();
            var itemType = type.GetElementType();
            var genericEnumType = typeof(IEnumerable<>).MakeGenericType(itemType);

            var dataAsEnum = data as IEnumerable;
            foreach (var item in dataAsEnum)
            {
                
            }
        }

        private static bool IsPrimitive(Type type)
        {
            return type.IsPrimitive;
        }
        
        private static bool IsValueType(Type type)
        {
            return type.IsValueType;
        }
        
        private static bool IsClass(Type type)
        {
            return type.IsClass;
        }
        
        private static bool IsArray(Type type)
        {
            return type.IsArray;
        }
        
        private static bool IsCollectible(Type type)
        {
            return type.IsCollectible;
        }
        
        
        
        private static bool IsIEnumerable(Type type)
        {
            return type.GetInterfaces().Contains(typeof(IEnumerable));
        }
    }

    public enum TestEnum
    {
        
    }
    
    public struct TestStruct
    {
        
    }
    
    public class TestClass
    {
        
    }
}