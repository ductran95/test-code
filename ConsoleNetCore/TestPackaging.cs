using System;
using System.IO;
using System.IO.Packaging;
using System.Reflection.Metadata;

namespace ConsoleNetCore
{
    public static class TestPackaging
    {


        public static void TestExistedPackage()
        {
            using (var package = OpenPackage(@"/home/ductran/Downloads/TEE-CLC-14.134.0.zip"))
            {
                var parts = package.GetParts();
                Console.Write("a");

            }
        }
        
        private static void CopyStream(Stream source, Stream target)
        {
            const int bufSize = 0x1000;
            byte[] buf = new byte[bufSize];
            int bytesRead = 0;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
                target.Write(buf, 0, bytesRead);
        }

        private static Package CreatePackage(string packagePath)
        {
            return Package.Open(packagePath, FileMode.OpenOrCreate);
        }
        
        private static Package OpenPackage(string packagePath)
        {
            return Package.Open(packagePath, FileMode.Open);
        }
    }
}