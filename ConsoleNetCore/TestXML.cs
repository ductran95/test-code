﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConsoleNetCore
{
    public static class TestXML
    {
        public static void Test()
        {
            // Creates a Book.
            var myBook = new Book();
            myBook.TITLE = "A Book Title";
            var myPrice = new Price();
            myPrice.price = (decimal)9.95;
            myPrice.currency = "US Dollar";
            myBook.PRICE = myPrice;
            myBook.ISBN = new ISBN()
            {
                Name = "book a123",
                Value = "123456",
                Data = "abcdef"
            };

            var myBooks = new Books();
            myBooks.BookData = new List<Book>{myBook, myBook};

            // var myBookXML = myBook.SerializeXml();
            // Console.WriteLine(myBookXML);

            var myBooksXML = myBooks.SerializeXml();
            Console.WriteLine(myBooksXML);
        }
    }

    [XmlRoot(ElementName = "newbooks")]
    public class Books
    {
        [XmlElement(ElementName = "book1")]
        public List<Book> BookData;
    }

    [XmlRoot(ElementName = "newbook")]
    public class Book
    {
        [XmlElement(ElementName = "title")]
        public string TITLE;
        
        [XmlElement(ElementName = "price")]
        public Price PRICE;
        
        [XmlElement(ElementName = "isbn")]
        public ISBN ISBN;
    }
    
    public class ISBN
    {
        [XmlText] 
        public string Value;
        
        [XmlElement] 
        public string Data;
        
        [XmlAttribute]
        public string Name;
    }

    public class Price
    {
        [XmlAttribute(Namespace = "http://www.cpandl.com")]
        public string currency;
        [XmlElement(Namespace = "http://www.cohowinery.com")]
        public decimal price;
    }

    public static class ObjectExtension
    {
        public static string SerializeXml<T>(this T data, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            using (var stringWriter = new StringWriterWithEncoding(encoding))
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringWriter, data);
                return stringWriter.ToString();
            }
        }

        public static T DeserializeXml<T>(this string xmlString)
        {
            using (var stringReader = new StringReader(xmlString))
            {
                var serializer = new XmlSerializer(typeof(T));
                var data = serializer.Deserialize(stringReader);
                return (T)data;
            }
        }
    }

    public class StringWriterWithEncoding : StringWriter
    {
        private Encoding _encoding;

        public override Encoding Encoding => _encoding;

        public StringWriterWithEncoding()
        {
            _encoding = Encoding.UTF8;
        }

        public StringWriterWithEncoding(Encoding encoding)
        {
            _encoding = encoding;
        }
    }
}
